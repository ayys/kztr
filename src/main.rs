use bilge::prelude::*;
use binread::BinRead;
use std::io::{self, Cursor};
use tokio::net::UdpSocket;

#[bitsize(16)]
#[derive(FromBits)]
struct HeaderFlags {
    qr: u1,
    opcode: u4,
    aa: u1,
    tc: u1,
    rd: u1,
    ra: u1,
    z: u3,
    rcode: u4,
}

#[derive(BinRead, Debug)]
#[br(big)]
struct DNSHeader {
    id: u16,
    flags: u16,
    qdcount: u16,
    ancount: u16,
    nscount: u16,
    arcount: u16,
}

#[tokio::main]
async fn main() -> io::Result<()> {
    // Bind UDP socket to localhost port 8080
    let sock = UdpSocket::bind("0.0.0.0:8080").await?;

    let mut buf = [0; 1024]; // Buffer to hold incoming data

    loop {
        // Receive data and store the source address
        let (len, addr) = sock.recv_from(&mut buf).await?;

        // Parse the DNS header from the first 12 bytes of the buffer
        let mut header_cursor = Cursor::new(&buf[..12]);
        let header: DNSHeader = DNSHeader::read(&mut header_cursor).unwrap();
        println!("Parsed DNS Header {:?}", header);
        let flags = HeaderFlags::from(header.flags);
        if flags.qr().into() {
            println!("This is a response");
        } else {
            println!("This is a query");
        }
        println!("QR is {}", flags.qr());
        println!("OPCODE is {}", flags.opcode());
        println!("AA is {}", flags.aa());
        println!("TC is {}", flags.tc());
        println!("RD is {}", flags.rd());
        println!("RA is {}", flags.ra());
        println!("Z is {}", flags.z());
        println!("RCODE is {}", flags.rcode());
        // Echo the data bayck to the sender
        let _len = sock.send_to(&buf[..len], &addr).await?;
    }
}
